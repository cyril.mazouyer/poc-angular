import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  // Liste des postes
  postsList: Array<Object> = [{
    title: 'Mon premier post',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur congue bibendum orci maximus egestas. Proin eu nunc at ante tempus hendrerit. Nullam blandit odio dui. Nunc blandit facilisis purus vitae accumsan.',
    loveIts: 0,
    created_at: new Date()
  }, {
    title: 'Mon deuxième post',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur congue bibendum orci maximus egestas. Proin eu nunc at ante tempus hendrerit. Nullam blandit odio dui. Nunc blandit facilisis purus vitae accumsan.',
    loveIts: 0,
    created_at: new Date()
  },{
    title: 'Encore un post',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur congue bibendum orci maximus egestas. Proin eu nunc at ante tempus hendrerit. Nullam blandit odio dui. Nunc blandit facilisis purus vitae accumsan.',
    loveIts: 0,
    created_at: new Date()
  }];

}
